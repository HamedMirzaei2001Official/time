class Time:
    all_times = list()
    count_times = int()

    def __init__(self, hour, minute, second):
        self.hour = hour
        self.minute = minute
        self.second = second
        self.all_times.append((self.hour, self.minute, self.second))
        Time.count_times += 1

    def set_time(self, hour, minute, second):
        self.set_hour(hour)
        self.set_minute(minute)
        self.set_second(second)

    def set_hour(self, hour):
        if hour >= 0 and hour <= 23 :
            self.hour = hour

    def set_minute(self, minute):
        if minute >= 0 and minute <= 59 :
            self.minute = minute

    def set_second(self, second):
        if second >= 0 and second <=59 :
            self.second = second

    def get_hour(self):
        return self.hour

    def get_minute(self):
        return self.minute

    def get_second(self):
        return self.second

    def get_time(self):
        return (self.get_hour(), self.get_minute(), self.get_second())

    def to_string(self):
        return f"{self.hour}:{self.minute}:{self.second}"

    @classmethod
    def get_all_times(cls):
        return cls.all_times

    @staticmethod
    def get_count_times():
        return Time.count_times


if __name__ == '__main__':
    t1 = Time(22, 3, 20)
    print(t1.get_time())
    t1.set_time(13, 45, 23)
    print(t1.to_string())
    t2 = Time(2, 55, 5)
    print(t2.get_hour())
    t2.set_minute(33)
    print(t2.to_string())
    print(Time.get_all_times())
    print(Time.get_count_times())